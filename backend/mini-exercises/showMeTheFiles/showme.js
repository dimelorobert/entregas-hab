'use strict';

// Modulos requeridos
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const files = process.argv.slice(2);

// Color rojo para los errores 
const colourError = chalk.red()

function showFilesPapu(filesList) {
  for (const file of filesList) {
    const filePath = path.resolve(file);
    try {
      if (fs.statSync(filePath).size < 10240) {
        const contentFile = fs.readFileSync(filePath, "utf-8");
        console.log(contentFile);
      } else {
        console.log(colourError(`The file ${file} is biggest than 10Kb.`));
      }
    } catch (error) {
      if (error.code === "ENOENT") {
        console.log(colourError(`The file ${file} does not exists.`));
      } else {
        console.log(colourError(error.message));
      }
    }
  }
}

showFilesPapu(files);