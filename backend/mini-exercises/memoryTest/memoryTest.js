"use strict";

// Modulos requeridos
const { totalmem, freemem } = require("os");
const chalk = require("chalk");

// Color chalk
const blue = chalk.blue();

console.log(
  `Tienes un ${blue(
    (freemem() * 100) / totalmem(),
    "%"
  )} de memoria libre.`
);