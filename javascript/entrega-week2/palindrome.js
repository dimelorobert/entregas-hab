////////////////////////////////// EJERCICIO CONTADOR LETRAS  /////////////////////////////////////////////

////////////////////////////////// EJERCICIO CONTADOR LETRAS  /////////////////////////////////////////////

/*Haz que la función PalindromeTwo(str) tome el parámetro str que se le pasa y devuelva true 
si el parámetro es un palíndromo, (la cadena se lee igual hacia adelante que hacia atrás) 
de lo contrario devuelve false. 
Por ejemplo: Arriba la birra debería devolver true, se lee igual del derecho que del revés.*/


'use strict';

const palindrome = '¡Arriba la birra!';

const char = /[.,\/#¡!¿?$%\^&\*;:{}=\-_`~()\s]/g;

function palindromeTwo(data) {
  const cleanString = data.toLowerCase().replace(char, '');
  const reversedString = cleanString
    .split('')
    .reverse()
    .join('');
  return cleanString === reversedString;
}

console.log(palindromeTwo(palindrome) + `: ${palindrome} ${palindromeTwo(palindrome) ? '' : ' no'} es palíndromo.`);