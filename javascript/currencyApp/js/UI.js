"use strict";

const urlAPI = "https://api.exchangerate-api.com/v4/latest/";
import {convertCurrency, getCurrencyList} from "./functions.js";

// Auno todos los elemntos a manipular en el DOM en una clase
class Interface {
  constructor(element, urlData) {
    this.element = element;
    this.urlData = urlData;
    this.form = this.element.querySelector("#form");
    this.ammountInput = this.element.querySelector("#number");
    this.selectFrom = this.element.querySelector("#selectCurrencyA");
    this.selectTo = this.element.querySelector("#selectCurrencyB");
    this.button = this.element.querySelector("#btn");
    this.convertionResult = this.element.querySelector("#result");
    this.convert();
  }

  // Metodo para rellenar los select con la lista de la API
  fillSelects(currencyList) {
    // Se crea fragment para ir almacenando las currencies en cada iteracion para no castigar a la memoria con las iteraciones
    const fragment = document.createDocumentFragment();
    for (const data of currencyList) {
      const option = document.createElement("option");
      option.setAttribute("value", data);
      option.textContent = data;
      fragment.append(option);
    }
    // Crea clon del fragment para selectTo
    const clonFragment = fragment.cloneNode(true);
    this.selectFrom.innerHTML = "";
    this.selectFrom.append(fragment);
    this.selectTo.innerHTML = "";
    this.selectTo.append(clonFragment);
  }

  // Agregando funcionalidad al boton CONVERT 
  convert() {
    this.form.addEventListener("submit", (event) => {
      event.preventDefault();
      const selectCurrencyA = this.form.elements.selectCurrencyA.value;
      const selectCurrencyB = this.form.elements.selectCurrencyB.value;
      const ammountInput = this.form.elements.number.value;    
      if (ammountInput) {
        convertCurrency(urlAPI, ammountInput, selectCurrencyA, selectCurrencyB).then(
          (result) => {
            const finalResult = result.toFixed(2);
            this.convertionResult.textContent = `${finalResult} ${selectCurrencyB}`;
          }
        );
        this.ammountInput.value = "";
        this.convertionResult.textContent = this.finalResult; 
      }
    });
  }
}

const converterInterface = new Interface(document.querySelector("#currency-app"), urlAPI);
getCurrencyList(urlAPI, "aed").then((response) => converterInterface.fillSelects(response));

