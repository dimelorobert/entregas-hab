"use strict";

//función para obtener la conversion de monedas
async function convertCurrency(urlData, ammount, selectCurrencyA, selectCurrencyB) {
  const ratiosList = await fetch(`${urlData + selectCurrencyA}`);
  const dataRatios = await ratiosList.json();
    return ammount * dataRatios.rates[selectCurrencyB];
}

// Funcion asyncrona generalizada para conseguir los ratios dependiendo de la base elegida previamente
async function getCurrencyList(urlData, rateCurrency) {
  const joinUrl = await fetch(`${urlData + rateCurrency}`);
  const response = await joinUrl.json();
    return Object.keys(response.rates);
}


export { convertCurrency, getCurrencyList};

