"use strict";

import { getCurrencyList } from "./functions.js";
import Interface from "./UI.js";

const urlAPI = "https://api.exchangerate-api.com/v4/latest/";
const converterInterface = new Interface(document.querySelector("#converter"), urlAPI);

getCurrencyList(urlAPI, "aed").then((response) => converterInterface.fillSelects(response));