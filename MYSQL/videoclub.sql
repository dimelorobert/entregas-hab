DROP DATABASE IF EXISTS videoclub; /*Ejecutar esta linea con CTRL + INTRO si se presenta algun error*/

CREATE DATABASE IF NOT EXISTS videoclub;
USE videoclub;

CREATE TABLE IF NOT EXISTS director (
id INT UNSIGNED AUTO_INCREMENT NOT NULL,
name VARCHAR(100) NOT NULL, 
PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS movie (
id INT UNSIGNED AUTO_INCREMENT NOT NULL,
name VARCHAR (100) NOT NULL,
description VARCHAR(255),
cover VARCHAR(255),
pegi INT, 
discount ENUM('CHEAP_OFFER', 'NORMAL_PRICE', 'RELEASED') NOT NULL, 
id_director INT UNSIGNED NOT NULL,
FOREIGN KEY fk_id_director (id_director) REFERENCES director(id) ,
PRIMARY KEY (id, id_director)
);

CREATE TABLE IF NOT EXISTS actor(
id INT UNSIGNED AUTO_INCREMENT NOT NULL,
id_movie INT UNSIGNED NOT NULL,
id_director INT UNSIGNED NOT NULL,
FOREIGN KEY fk_id_movie(id_movie) REFERENCES movie(id),
FOREIGN KEY fk_id_director (id_director) REFERENCES director(id), 
PRIMARY KEY(id, id_movie, id_director)
);

CREATE TABLE IF NOT EXISTS distribuitor(
id INT UNSIGNED AUTO_INCREMENT NOT NULL,
name VARCHAR(50) NOT NULL,
telephone VARCHAR(30),
dni VARCHAR(20) NOT NULL,
email VARCHAR(100),
PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS copy(
id INT UNSIGNED AUTO_INCREMENT NOT NULL, 
format ENUM('DVD','VHS') NOT NULL,
available BOOLEAN NOT NULL,
id_movie INT UNSIGNED NOT NULL, 
id_distribuitor INT UNSIGNED NOT NULL, 
FOREIGN KEY fk_id_movie(id_movie) REFERENCES movie(id),
FOREIGN KEY fk_id_distribuitor(id_distribuitor) REFERENCES distribuitor(id),
PRIMARY KEY(id, id_movie, id_distribuitor)
);

CREATE TABLE IF NOT EXISTS user(
id INT UNSIGNED AUTO_INCREMENT NOT NULL,
name VARCHAR(255) NOT NULL,
dni VARCHAR(20) NOT NULL,
telephone VARCHAR(30),
email VARCHAR(100),
dob DATE NOT NULL, 
PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS rent(
id INT UNSIGNED AUTO_INCREMENT NOT NULL,
id_copy INT UNSIGNED  NOT NULL, 
id_user INT UNSIGNED  NOT NULL, 
start_date DATETIME NOT NULL,
end_date DATETIME NOT NULL,
quialification DECIMAL(2,1),
reviews VARCHAR(255),
FOREIGN KEY fk_id_copy(id_copy) REFERENCES copy(id),
FOREIGN KEY fk_id_user(id_user) REFERENCES user(id), 
PRIMARY KEY(id, id_copy, id_user)
);